package com.example.myproject.view

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.belajarandroid.base.PreferencesHelper
import com.example.myproject.R
import com.example.myproject.base.BaseDialog

import com.example.myproject.databinding.ActivityProfileBinding
import com.example.myproject.view.MainActivity
import com.example.myproject.view.MainActivity.Companion.KEY_ADDRESS
import com.example.myproject.view.MainActivity.Companion.KEY_INPUT_EMAIL
import com.example.myproject.view.MainActivity.Companion.KEY_INPUT_PASSWORD
import com.example.myproject.view.MainActivity.Companion.KEY_NAME
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProfileBinding

    @Inject
    lateinit var preferencesHelper: PreferencesHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

//        val email = intent.getStringExtra(KEY_INPUT_EMAIL)
//        val password = intent.getStringExtra(KEY_INPUT_PASSWORD)
//        val name = intent.getStringExtra(KEY_NAME)
//        val address = intent.getStringExtra(KEY_ADDRESS)

        getData()
        binding.btnLogout.setOnClickListener {
            showDialogLogout()
        }

        binding.btnNewsItem.setOnClickListener {
            val intent = Intent(applicationContext, HomeMainActivity::class.java)
            startActivity(intent)
        }

    }

    private fun showDialogLogout() {
        val dialog = BaseDialog(
            this,
            "Warning",
            "Are you sure logout?",
            onClicked = {
                Toast.makeText(this, "ini button Procced", Toast.LENGTH_LONG).show()
                logout()
            },
            withImage = true,
            image = R.drawable.ic_warning
        )
        dialog.setCancelable(false)
        dialog.show()
    }

    private fun logout() {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }

    private fun getData() {
        val nameValue = preferencesHelper.getName()
        val addressValue = preferencesHelper.getAddress()
        val emailValue = preferencesHelper.getEmail()
        var passwordValue = preferencesHelper.getPassword()

        passwordValue = passwordValue?.replace(passwordValue, "********")
        binding.tvEmail.text = emailValue
        binding.tvPassword.text = passwordValue
        binding.tvName.text = nameValue
        binding.tvAddress.text = addressValue
    }
}