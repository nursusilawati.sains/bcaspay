package com.example.myproject.view

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryModel(
    val image: String?,
    val title:String,
    val subtitle: String

) : Parcelable