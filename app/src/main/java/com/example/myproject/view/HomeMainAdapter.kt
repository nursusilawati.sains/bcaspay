package com.example.myproject.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myproject.databinding.ItemNewsBinding

class HomeMainAdapter(
    private var dataNews: MutableList<NewsModel> = mutableListOf()

) :
    RecyclerView.Adapter<HomeMainAdapter.HomeMainViewHolder> (){

    fun addDataNews(newData: List<NewsModel>) {
        dataNews.addAll(newData)
        notifyDataSetChanged()
    }
    inner class HomeMainViewHolder(var binding: ItemNewsBinding):RecyclerView.ViewHolder(
        binding.root
    ){

        fun bindView(data : NewsModel) {
            Glide.with(binding.root.context)
                .load(data.image)
                .into(binding.ivItemNews)

            binding.tvTitleNewsh2.text = data.title
            binding.tvSubtitleNews2.text = data.subtitle

//            binding.lvNews.setOnClickListener {
//                onClickNews(data)
//            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeMainViewHolder
            = HomeMainViewHolder(
        ItemNewsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount(): Int = dataNews.size

    override fun onBindViewHolder(holder: HomeMainViewHolder, position: Int) {
        holder.bindView(dataNews[position])
    }
}