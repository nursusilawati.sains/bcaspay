package com.example.myproject.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Email
import android.provider.ContactsContract.CommonDataKinds.SipAddress
import android.widget.Toast
import com.example.belajarandroid.base.PreferencesHelper
import com.example.myproject.databinding.ActivityMainBinding
import com.example.myproject.view.RegistrasiActivity.Companion.inputNameReg
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val name= "John Doe"
    private val address = "Indonesia"
    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var preferencesHelper: PreferencesHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        binding.btnLogin.setOnClickListener {
            val inputEmail = binding.etEmail.text.toString()
            val inputPassword = binding.etPassword.text.toString()
            if (inputEmail.isEmpty().not() or  inputPassword.isEmpty().not()) {
                val getName = preferencesHelper.getName()
                if (getName == inputNameReg){
                    preferencesHelper.saveName(getName)
                }else{
                    preferencesHelper.saveName(name)
                }
                preferencesHelper.saveAddress(address)
                preferencesHelper.saveEmail(inputEmail)
                preferencesHelper.savePassword(inputPassword)
                navigateScreenWithInput(ProfileActivity::class.java)
                Toast.makeText(applicationContext, "Login Berhasil", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(applicationContext, "Silahkan isi email atau password", Toast.LENGTH_LONG).show()

            }
        }

            binding.btnRegister.setOnClickListener {
                val intent = Intent(applicationContext, RegistrasiActivity::class.java)
                startActivity(intent)
            }
        }
    private fun navigateScreenWithInput(screen : Class<*>) {
        val intent = Intent(applicationContext, screen)
//        intent.putExtra(KEY_INPUT_EMAIL, email)
//        intent.putExtra(KEY_INPUT_PASSWORD, password)
//        intent.putExtra(KEY_NAME, name)
//        intent.putExtra(KEY_ADDRESS, address)
        startActivity(intent)
    }
    companion object {
        const val KEY_INPUT_EMAIL = "email"
        const val KEY_INPUT_PASSWORD = "password"
        const val KEY_NAME = "name"
        const val KEY_ADDRESS = "address"
    }
}