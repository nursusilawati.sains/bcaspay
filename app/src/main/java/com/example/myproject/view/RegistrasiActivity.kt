package com.example.myproject.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.isDigitsOnly
import com.example.belajarandroid.base.PreferencesHelper
import com.example.myproject.databinding.ActivityRegistrasiBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RegistrasiActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegistrasiBinding

    private val address = "Indonesia"
    @Inject
    lateinit var preferencesHelper: PreferencesHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistrasiBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        binding.btnCreate.setOnClickListener {
            inputNameReg = binding.etName.text.toString()
            val inputEmail = binding.etEmail.text.toString()
            val inputPassword = binding.etPassword.text.toString()
            val intent = Intent(applicationContext, MainActivity::class.java)
            if (inputPassword.isEmpty().not() or inputEmail.isEmpty().not() or inputNameReg.isEmpty().not()) {
                preferencesHelper.saveName(inputNameReg)
                preferencesHelper.saveEmail(inputEmail)
                preferencesHelper.saveAddress(address)
                preferencesHelper.savePassword(inputPassword)
                startActivity(intent)
                Toast.makeText(applicationContext, "Create Success, silahkan login", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(applicationContext, "kolom tidak boleh kosong", Toast.LENGTH_LONG).show()
                }
        }
    }
    companion object{
        var inputNameReg = ""
    }
}