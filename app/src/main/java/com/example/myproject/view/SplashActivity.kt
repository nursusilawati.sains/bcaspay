package com.example.myproject.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myproject.databinding.ActivitySplashBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {
        private lateinit var binding: ActivitySplashBinding
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            binding = ActivitySplashBinding.inflate(layoutInflater)
            setContentView(binding.root)
            supportActionBar?.hide()
            CoroutineScope(Dispatchers.Main).launch {
                delay(3000L)
                val intent = Intent (applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()
            }


        }
    }
