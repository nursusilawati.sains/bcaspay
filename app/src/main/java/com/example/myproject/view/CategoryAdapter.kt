package com.example.myproject.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myproject.view.CategoryModel
import com.example.myproject.databinding.ItemCategoryBinding

class CategoryAdapter (
    private var data: MutableList<CategoryModel> = mutableListOf()
): RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false)
        )
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bindView(data[position])

    }
    override fun getItemCount(): Int {
        return data.size
    }

    inner class CategoryViewHolder(var binding: ItemCategoryBinding):
        RecyclerView.ViewHolder(binding.root){
        fun bindView(data : CategoryModel) {
            Glide.with(binding.root.context)
                .load(data.image)
                .into(binding.ivItemNewsh)
            binding.tvTitleNewsh2.text = data.title
            binding.tvSubtitleNewsh2.text = data.subtitle
        }
    }

}