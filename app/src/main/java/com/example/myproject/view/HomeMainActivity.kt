package com.example.myproject.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myproject.view.CategoryModel
import com.example.myproject.R
import com.example.myproject.databinding.ActivityHomeBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class HomeMainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        val mainAdapter = HomeMainAdapter(populateData())
//            dataNews = populateData(),
//            onClickNews ={dataNews ->
//                DetailNewsActivity.navigateToActivityDetail(activity = this, dataNews= dataNews)
//            })

        binding.rvNews.adapter = mainAdapter

        val categoryAdapter = CategoryAdapter(populateDataForCategory())
        binding.rvListHorizontal.adapter = categoryAdapter
        binding.ivUser.setOnClickListener {
            val intent = Intent(applicationContext, ProfileActivity::class.java)
            startActivity(intent)
        }
    }


    private fun populateData(): MutableList<NewsModel> {
        return mutableListOf(
            NewsModel(
                image = "https://i0.wp.com/harga.web.id/wp-content/uploads/tahapan-ib-bca-syariah.jpg?resize=680%2C300&ssl=1",
                title = "Tahapan iB",
                subtitle = "Tahapan iB adalah rekening tabungan yang menyediakan berbagai fasilitas yang memudahkan anda dalam transaksi perbankan berdasarkan prinsip wadiah yad dhamanah atau mudharabah muthlaqah"
            ),
            NewsModel(
                image = "https://fesyarjawa.com/asset/produk/original/p1gc3kj4ppb1tq7c17gdcppqshl.jpg",
                title = "Tahapan Mabrur iB",
                subtitle ="Tahapan Mabrur iB adalah tabungan berdasarkan prinsip bagi hasil (mudharabah mutlaqoh) yang bertujuan untuk membantu nasabah dalam mewujudkan rencana ibadah Umrah dan Haji",
            ),
            NewsModel(
                image = "https://1.bp.blogspot.com/-FQuji3s3kGY/XcGCTkQzdRI/AAAAAAAABe0/nPycD-N1ZScfZ82wdM0vgal-pF6z26hqwCLcBGAsYHQ/s400/Tabungan%2BSimpel%2BDan%2BSimpel%2BiB%2BUntuk%2BPelajar.jpg",
                title = "SimPel ib",
                subtitle = "Tabungan untuk siswa/siswi yang diterbitkan secara bersama oleh bank-bank di Indonesia dalam rangka edukasi dan inklusi keuangan dan mendorong budaya menabung sejak dini. SimPel iB adalah tabungan dengan persyaratan mudah dan sederhana, serta fitur yang menarik dengan pilhan akad wadiah yad dhamanah atau mudharabah muthlaqah",
            ),
            NewsModel(
                image = "https://bankir.id/wp-content/uploads/2020/11/Kelebihan-Kekurangan-Deposito-IB-BCA-Syariah.jpg",
                title = "Deposito iB",
                subtitle = "Deposito iB adalah solusi untuk berinvestasi dengan bagi hasil yang kompetitif berdasarkan prinsip mudharabah muthlaqah\n" +
                        "Manfaat\n" +
                        "\n" +
                        "Anda dapat memilih sendiri jangka waktu deposito sesuai dengan keinginan\n" +
                        "Anda bisa memindahkan bagi hasil Deposito iB Anda secara otomatis ke rekening Giro iB/Tahapan iB\n" +
                        "Tersedia pilihan perpanjangan sesuai kebutuhan",
            ),
            NewsModel(
                image = "https://www.viralorchard.com/wp-content/uploads/2020/06/Buka-Rekening-BCA-Syariah.jpg",
                title = "Rekening Dana Nasabah",
                subtitle = "Rekening Dana Nasabah (RDN) adalah rekening yang digunakan oleh nasabah untuk penyelesaian transaksi efek dengan akad Mudharabah Muthlaqah atau wadiah yad dhamanah",
            )
        )
    }

    private fun populateDataForCategory(): MutableList<CategoryModel> {
        return mutableListOf(
            CategoryModel(
                image = "https://fesyarjawa.com/asset/produk/original/p1gc3km21dsdp1gd51n251r6g1l07l.jpg",
                title = "Pembiayaan KKB iB",
                subtitle = "Layanan pembiyaan KKB iB BCA Syariah merupakan pembiayaan yang diberikan BCA Syariah kepada nasabah berdasarkan prinsip Syariah dengan tujuan untuk kepemilikan atau pembelian kendaraan bermotor baru atau bekas.",
            ),
            CategoryModel(
                image = "https://blogger.googleusercontent.com/img/a/AVvXsEgtHo7Mb_6e86o_z_FAArIrOwXTKqaZFu7rBZ97yHzIF_u0lgDvH4GvTd1OHsGbxmwaH1DKkQYiRoStKh_kyDMty2j4RznHFBPON3IrWoM-hkdmwzpFDbGsIxEFQTRjdaGDOXS-8jmoJ192BoFNcH_nbFdTtP3EJVgr58qswinv4bRScClbKd7p9w5QBg=s16000",
                title = "Pembiayaan KPR iB",
                subtitle ="Pembiayaan KPR iB adalah pembiayaan pembelian rumah tinggal/apartemen berdasarkan prinsip murabahah. KPR iB BCA Syariah bisa untuk pembelian rumah ready stock, rumah inden (untuk developer yang sudah bekerjasama dengan BCA Syariah) atau take over KPR dengan cicilan ringan, pasti tanpa biaya-biaya di muka.",
            ),
            CategoryModel(
                image = "https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full//90/MTA-5522903/bca_syariah_bca_syariah_quad_-_9d_umrah_by_oman_air_-keberangkatan_22_februari_2020-_full02_owfrglrv.jpg",
                title = "Pembiayaan Umrah iB",
                subtitle = "BCA Syariah bekerja sama dengan Tour & Travel pilihan menyediakan paket Umrah Eksklusif dengan harga terjangkau khusus bagi nasabah BCA Syariah. Pembiayaan Umrah iB adalah salah satu fasilitas pembiayaan multijasa dengan akad sewa-menyewa (Ijarah Multijasa) untuk membantu nasabah mewujudkan niat melaksanakan ibadah umrah.",
            ),
            CategoryModel(
                image = "https://fesyarjawa.com/asset/produk/original/p1gc3kmfva1hm5bkbe5o1og745sl.jpg",
                title = "Pembiayaan Emas iB",
                subtitle ="Pembiayaan Emas iB merupakan produk pembiayaan dari BCA Syariah untuk kepemilikan Logam Mulia (Emas) dengan prinsip Syariah. Proses cepat & mudah, Akad murabahah (jual beli), Jumlah pembiayaan mulai dari setara 10 Gr logam mulia Antam, Jangka waktu pembiayaan maksimal 5 tahun,Uang muka minimal 10% dari harga beli logam mulia",
            ),
            CategoryModel(
                image = "https://fesyarjawa.com/asset/produk/original/p1gc3khmf61epse1u1advmnm6d6l.jpg",
                title = "Pembiayaan Modal Kerja iB",
                subtitle = "Merupakan penyediaan dana jangka pendek atau menengah berdasarkan prinsip syariah untuk membantu usaha nasabah dalam memenuhi kebutuhan modal kerja seperti penyediaan barang dagangan, bahan baku, dan kebutuhan modal kerja lainnya",
            ),
            CategoryModel(
                image = "https://bankir.id/wp-content/uploads/2020/11/Tabungan-Mabrur-iB-BCA-Syariah.jpg",
                title = "Layanan Setoran BPIH",
                subtitle ="BCA Syariah kini memiliki Layanan Penerimaan Setoran Biaya Penyelenggaraan Ibadah Haji (LPS BPIH) untuk kemudahan pembayaran setoran awal dan setoran pelunasan biaya ibadah Haji. Layanan Penerimaan Setoran Biaya Penyelenggaraan Ibadah Haji (LPS BPIH) ini memiliki beberapa manfaat diantaranya, Terkoneksi dengan SISKOHAT KEMENAG (Sistem Komputerisasi Haji Terpadu Kementerian Agama)\n" +
                        "Sumber dana bisa secara tunai atau debet rekening di BCA Syariah, Mudah mempersiapkan setoran BPIH (Biaya Penyelenggaraan Ibadah Haji) melalui Tahapan Mabrur iB yang menawarkan berbagai keunggulan",
            ),
            CategoryModel(
                image = "https://fesyarjawa.com/asset/produk/original/p1gc3kio1tu3r1les6qi1mc91etsl.jpg?aaa",
                title = "BCA Syariah Mobile",
                subtitle = "BCA Syariah Mobile merupakan fasilitas perbankan elektronik dari BCA Syariah untuk kemudahan nasabah mengakses rekening yang dimiliki atau melakukan transaksi perbankan melalui smartphone dengan menggunakan jaringan internet meliputi transaksi finansial dan transaksi non finansial.",
            ),

            )
    }
}