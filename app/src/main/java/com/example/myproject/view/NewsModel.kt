package com.example.myproject.view

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsModel(
    val image: String?,
    val title: String,
    val subtitle: String
    ) : Parcelable