package com.example.belajarandroid.base

import android.content.SharedPreferences
import javax.inject.Inject

class PreferencesHelper @Inject constructor(private val sharedPreferences: SharedPreferences) {
    fun saveName(value: String){
        sharedPreferences.edit().putString(KEY_NAME, value).apply()
    }
    fun getName(): String?{
        return sharedPreferences.getString(KEY_NAME, "-")
    }
    fun saveEmail(value: String){
        sharedPreferences.edit().putString(KEY_EMAIL, value).apply()
    }
    fun getEmail(): String?{
        return sharedPreferences.getString(KEY_EMAIL, "-")
    }
    fun savePassword(value: String){
        sharedPreferences.edit().putString(KEY_PASSWORD, value).apply()
    }
    fun getPassword(): String?{
        return sharedPreferences.getString(KEY_PASSWORD, "-")
    }
    fun saveAddress(value: String){
        sharedPreferences.edit().putString(KEY_ADDRESS, value).apply()
    }
    fun getAddress(): String?{
        return sharedPreferences.getString(KEY_ADDRESS, "-")
    }
    fun clearDatePref(){
        sharedPreferences.edit().clear().apply()
    }
    companion object{
        const val KEY_NAME = "key_name"
        const val KEY_EMAIL = "key_email"
        const val KEY_PASSWORD = "key_password"
        const val KEY_ADDRESS = "key_address"
    }
}