package com.example.myproject.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.core.view.isVisible
import com.example.myproject.R
import com.example.myproject.databinding.BaseDialogBinding

class BaseDialog(context: Context,
                 val title: String,
                 val subtitle: String,
                 private val onClicked: (()-> Unit),
                 private val withImage: Boolean,
                 private val image: Int? = null
) : Dialog(context) {

    private  lateinit var binding : BaseDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = BaseDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivDialog.setImageResource(image ?: R.drawable.ic_warning)

        binding.ivDialog.isVisible = withImage

        binding.tvDialogTitle.text = title
        binding.tvDialogSubtitle.text = subtitle

        binding.btnCancel.setOnClickListener { dismissdialog() }
        binding.btnProcced.setOnClickListener {
            onClicked.invoke()
            dismiss()
        }
    }
    private fun dismissdialog(){
        dismiss()
    }
}